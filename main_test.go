package main

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestReadConfigFromEnvironment(t *testing.T) {
	tt := map[string]struct {
		name, port, host, dbName, user, pwd, expectedError string
	}{
		"test with all env variables set": {
			port:   "1234",
			host:   "localhost",
			dbName: "test",
			user:   "test",
			pwd:    "test",
		}, /*
			"fails while port is not set": {
				expectedError: "Port definition not found in the environment",
			},*/
		"fails while host is not set": {
			port:          "1234",
			expectedError: "Host definition not found in the environment",
		},
		"fails while db name is no set": {
			port:          "1234",
			host:          "localhost",
			expectedError: "Database Name definition not found in the environment",
		},
		"fails while user is no set": {
			port:          "1234",
			host:          "localhost",
			dbName:        "test",
			expectedError: "User definition not found in the environment",
		},
		"fails while password is not set": {
			port:          "1234",
			host:          "localhost",
			dbName:        "test",
			user:          "test",
			expectedError: "Users Password definition not found in the environment",
		},
	}
	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			//os.Setenv(EnvDbPort, tc.port)
			os.Setenv(EnvDbHost, tc.host)
			os.Setenv(EnvDbName, tc.dbName)
			os.Setenv(EnvDbUser, tc.user)
			os.Setenv(EnvDbPwd, tc.pwd)
			cfg, err := readDbConfigFromEnv()
			if len(tc.expectedError) != 0 {
				assert.EqualError(t, err, tc.expectedError)
			} else {
				//assert.Equal(t, tc.port, cfg.dbPort)
				assert.Equal(t, tc.host, cfg.dbHost)
				assert.Equal(t, tc.dbName, cfg.dbName)
				assert.Equal(t, tc.user, cfg.dbUser)
				assert.Equal(t, tc.pwd, cfg.dbPwd)
			}

		})
	}
}
