package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/d-hassel-home/timebandit-api/api"
	"gitlab.com/d-hassel-home/timebandit-api/service"
	"log"
	"net/http"
	"os"
)

const (
	apiversion = "v1"
	// EnvDbPort is the environment Name for the db port
	EnvDbPort = "DB_PORT"
	// EnvDbHost is the environment Name for the db host
	EnvDbHost = "DB_HOST"
	// EnvDbName is the environment Name for the db host
	EnvDbName = "DB_NAME"
	// EnvDbUser is the environment Name for the db user
	EnvDbUser = "DB_USER"
	// EnvDbPwd is the environment Name for the db password
	EnvDbPwd = "DB_PWD"
)

type config struct {
	dbPort, dbHost, dbName, dbUser, dbPwd string
}

func readDbConfigFromEnv() (config, error) {
	log.Println("reading configuration from environment")
	var cfg config
	/*
		cfg.dbPort = os.Getenv(EnvDbPort)
		if cfg.dbPort == "" {
			return cfg, fmt.Errorf("Port definition not found in the environment")
		}*/

	cfg.dbHost = os.Getenv(EnvDbHost)
	if cfg.dbHost == "" {
		return cfg, fmt.Errorf("Host definition not found in the environment")
	}

	cfg.dbName = os.Getenv(EnvDbName)
	if cfg.dbName == "" {
		return cfg, fmt.Errorf("Database Name definition not found in the environment")
	}

	cfg.dbUser = os.Getenv(EnvDbUser)
	if cfg.dbUser == "" {
		return cfg, fmt.Errorf("User definition not found in the environment")
	}

	cfg.dbPwd = os.Getenv(EnvDbPwd)
	if cfg.dbPwd == "" {
		return cfg, fmt.Errorf("Users Password definition not found in the environment")
	}

	return cfg, nil
}

func main() {
	log.Println("Starting the application")

	log.Println("Read database config ...")
	dbcfg, err := readDbConfigFromEnv()
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Create database service ...")
	projectService := service.NewProjectService()
	s := []service.DBService{projectService}

	r, err := service.New(s, service.WithDatabaseCredentials(dbcfg.dbUser, dbcfg.dbPwd, dbcfg.dbName, dbcfg.dbHost))
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()

	log.Println("Initialize router ...")
	router := mux.NewRouter()

	healthRoute := router.PathPrefix("/").Subrouter()
	api.RegisterHealthRoutes(healthRoute)

	root := router.PathPrefix(fmt.Sprintf("/%s", apiversion)).Subrouter()
	projectAPI := api.NewProjectAPI(projectService)
	projectAPI.RegisterProjectRoutes(root)

	http.ListenAndServe(":8080", router)
}
