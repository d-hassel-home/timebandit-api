package api

import (
	"encoding/json"
	"github.com/asaskevich/govalidator"
	"net/http"
	"strings"
)

func (p *projectAPI) createProject(w http.ResponseWriter, r *http.Request) (interface{}, int, *Error) {
	var m sendProjectDescription
	err := json.NewDecoder(r.Body).Decode(&m)
	if err != nil {
		return nil, http.StatusBadRequest, NewError(nil, "request does not contain valid json", http.StatusBadRequest)
	}
	defer r.Body.Close()

	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		msgs := []string{}
		multiple := err.(govalidator.Errors).Errors()

		for _, e := range multiple {
			msgs = append(msgs, e.Error())
		}

		return nil, http.StatusBadRequest, NewError(nil, strings.Join(msgs, ";"), http.StatusBadRequest)
	}

	project, err := p.service.CreateProject(m.Name, m.Description)
	if err != nil {
		return nil, http.StatusBadRequest, NewError(err, err.Error(), http.StatusBadRequest)
	}
	return project, http.StatusCreated, nil
}
