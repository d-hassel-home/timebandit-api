package api

import (
	"net/http"
)

func (p *projectAPI) listProjects(w http.ResponseWriter, r *http.Request) (interface{}, int, *Error) {
	name := r.FormValue("name")
	if len(name) == 0 {
		return nil, http.StatusBadRequest, NewError(nil, "Missing name parameter", http.StatusBadRequest)
	}
	projects, err := p.service.FindProjects(name)
	if err != nil {
		return nil, http.StatusBadRequest, NewError(err, err.Error(), http.StatusBadRequest)
	}
	return projects, http.StatusOK, nil
}
