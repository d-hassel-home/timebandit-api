package api_test

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/d-hassel-home/timebandit-api/api"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHealthCheck(t *testing.T) {
	expectedStatus := 200
	expectedResult := struct {
		Message string `json:"message"`
	}{Message: "I am alive!"}
	router := mux.NewRouter()
	api.RegisterHealthRoutes(router)
	srv := httptest.NewServer(router)
	defer srv.Close()
	r, err := http.Get(srv.URL + "/health")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, expectedStatus, r.StatusCode)

	data, err := ioutil.ReadAll(r.Body)

	if err != nil {
		t.Errorf("found unexpected error %v", err)
	}
	var result struct {
		Message string `json:"message"`
	}

	err = json.Unmarshal(data, &result)

	if err != nil {
		t.Errorf("found unexpected error %v", err)
	}
	assert.Equal(t, expectedResult, result)
}
