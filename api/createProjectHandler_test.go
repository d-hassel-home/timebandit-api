package api_test

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/d-hassel-home/timebandit-api/api"
	"gitlab.com/d-hassel-home/timebandit-api/service"
	"gitlab.com/d-hassel-home/timebandit-common/model"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestCreateProject(t *testing.T) {
	tt := map[string]struct {
		name           string
		data           string
		create         func(shortName string, longName string) (*model.Project, error)
		expectedError  string
		expectedStatus int
		expectedData   *model.Project
	}{
		"create project with 201": {
			data: "{\"Name\":\"test\",\"Description\":\"test long\"}}",
			create: func(shortName string, longName string) (*model.Project, error) {
				return &model.Project{ShortName: "test", LongName: "test long"}, nil
			},
			expectedStatus: 201,
			expectedData:   &model.Project{ShortName: "test", LongName: "test long"},
		},
		"create project with service error": {
			data: "{\"Name\":\"test\",\"Description\":\"test long\"}}",
			create: func(shortName string, longName string) (*model.Project, error) {
				var prj model.Project
				return &prj, fmt.Errorf("test error")
			},
			expectedStatus: 400,
			expectedError:  "test error\n",
		},
		"create project with 400 for wrong data send": {
			data:           "test",
			expectedStatus: 400,
			expectedError:  "request does not contain valid json\n",
		},
		"create project with 400 for wrong data format": {
			data:           "{\"Test\":\"test\"}",
			expectedStatus: 400,
			expectedError:  "name is a required field and is empty\n",
		},
	}
	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			mock := service.MockProjectService{Create: tc.create}
			api := api.NewProjectAPI(&mock)
			router := mux.NewRouter()
			api.RegisterProjectRoutes(router)
			srv := httptest.NewServer(router)
			defer srv.Close()
			r, err := http.Post(srv.URL+"/projects", "application/json", strings.NewReader(tc.data))
			assert.Equal(t, tc.expectedStatus, r.StatusCode)
			if err != nil {
				t.Errorf("found unexpected error %v", err)
			}
			data, err := ioutil.ReadAll(r.Body)
			if err != nil {
				t.Errorf("found unexpected error %v", err)
			}
			if len(tc.expectedError) > 0 {
				assert.Equal(t, tc.expectedError, string(data))
			} else {
				var p model.Project

				err = json.Unmarshal(data, &p)

				assert.Equal(t, *tc.expectedData, p)
			}
		})
	}
}
