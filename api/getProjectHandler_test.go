package api_test

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/d-hassel-home/timebandit-api/api"
	"gitlab.com/d-hassel-home/timebandit-api/service"
	"gitlab.com/d-hassel-home/timebandit-common/model"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestGetProject(t *testing.T) {
	tt := map[string]struct {
		name           string
		data           string
		get            func(uuid uuid.UUID) (*model.Project, error)
		expectedError  string
		expectedStatus int
		expectedData   *model.Project
	}{
		"get project with 200": {
			data: "d3ce797e-f409-4e26-a401-9b060c002627",
			get: func(id uuid.UUID) (*model.Project, error) {
				return &model.Project{UUID: id, ShortName: "test", LongName: "test long", Created: time.Now().UTC()}, nil
			},
			expectedStatus: 200,
			expectedData:   &model.Project{ShortName: "test", LongName: "test long"},
		},
		"get project with service error": {
			data: "d3ce797e-f409-4e26-a401-9b060c002627",
			get: func(uuid uuid.UUID) (*model.Project, error) {
				var prj model.Project
				return &prj, fmt.Errorf("test error")
			},
			expectedStatus: 400,
			expectedError:  "test error\n",
		},
		"get project with no data": {
			data: "d3ce797e-f409-4e26-a401-9b060c002627",
			get: func(uuid uuid.UUID) (*model.Project, error) {
				return nil, nil
			},
			expectedStatus: 404,
			expectedData:   &model.Project{ShortName: "", LongName: ""},
		},
		"get project with failed uuid": {
			data:           "failed",
			expectedStatus: 400,
			expectedError:  "invalid UUID length: 6\n",
		},
	}
	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			mock := service.MockProjectService{Get: tc.get}
			api := api.NewProjectAPI(&mock)
			router := mux.NewRouter()
			api.RegisterProjectRoutes(router)
			srv := httptest.NewServer(router)
			defer srv.Close()
			r, err := http.Get(srv.URL + "/projects/" + tc.data)
			assert.Equal(t, tc.expectedStatus, r.StatusCode)
			if err != nil {
				t.Errorf("found unexpected error %v", err)
			}
			data, err := ioutil.ReadAll(r.Body)
			if err != nil {
				t.Errorf("found unexpected error %v", err)
			}
			if len(tc.expectedError) > 0 {
				assert.Equal(t, tc.expectedError, string(data))
			} else {
				var p model.Project

				err = json.Unmarshal(data, &p)

				assert.Equal(t, tc.expectedData.LongName, p.LongName)
				assert.Equal(t, tc.expectedData.ShortName, p.ShortName)
			}
		})
	}
}
