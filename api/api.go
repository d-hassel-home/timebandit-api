package api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

// Error is spezialized for http error handling
type Error struct {
	err     error
	message string
	code    int
}

func (e Error) Error() string {
	return e.message
}

// NewError creates a new APIError with message, http code and original error
func NewError(err error, message string, code int) *Error {
	return &Error{err: err, message: message, code: code}
}

type handle func(w http.ResponseWriter, r *http.Request) (interface{}, int, *Error)

func (fn handle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	data, status, err := fn(w, r)
	if err != nil { // e is *APIError, not os.Error.
		http.Error(w, err.message, err.code)
		return
	}
	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(status)

	e := json.NewEncoder(w).Encode(data)
	if e != nil {
		m := fmt.Sprintf("could not encode the response: %v", e)
		http.Error(w, m, 500)
	}
}

func logger(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		inner.ServeHTTP(w, r)

		log.Printf(
			"%s\t%s\t%s\t%s",
			r.Method,
			r.RequestURI,
			name,
			time.Since(start),
		)
	})
}
