package api

import (
	"github.com/gorilla/mux"
	"net/http"
)

// RegisterHealthRoutes registers all health routes to the given router
func RegisterHealthRoutes(router *mux.Router) {
	router.StrictSlash(true)

	router.Handle("/health", logger(handle(healthCheckResponse), "health")).Methods("GET")
}

func healthCheckResponse(w http.ResponseWriter, r *http.Request) (interface{}, int, *Error) {
	return struct {
		Message string `json:"message"`
	}{Message: "I am alive!"}, http.StatusOK, nil
}
