package api

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"net/http"
)

func (p *projectAPI) deleteProject(w http.ResponseWriter, r *http.Request) (interface{}, int, *Error) {

	vars := mux.Vars(r)
	uid := vars["uuid"]
	uuid, err := uuid.Parse(uid)
	if err != nil {
		return nil, http.StatusBadRequest, NewError(err, err.Error(), http.StatusBadRequest)
	}
	err = p.service.DeleteProject(uuid)
	if err != nil {
		return nil, http.StatusBadRequest, NewError(err, err.Error(), http.StatusBadRequest)
	}
	return fmt.Sprintf("Project with uuid %s deleted", uuid), http.StatusOK, nil
}
