package api_test

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/d-hassel-home/timebandit-api/api"
	"gitlab.com/d-hassel-home/timebandit-api/service"
	"gitlab.com/d-hassel-home/timebandit-common/model"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestListProjects(t *testing.T) {
	tt := map[string]struct {
		name           string
		query          string
		find           func(name string) ([]model.Project, error)
		expectedError  string
		expectedStatus int
		expectedData   func() []model.Project
	}{
		"list projects with 200": {
			query: "?name=test",
			find: func(name string) ([]model.Project, error) {
				data := make([]model.Project, 1)
				id, _ := uuid.Parse("d3ce797e-f409-4e26-a401-9b060c002627")
				//data[0] = model.Project{UUID: id, ShortName: "test", LongName: "test long", Created: time.Now().UTC()}
				data = append(data, model.Project{UUID: id, ShortName: "test", LongName: "test long", Created: time.Now().UTC()})
				return data, nil
			},
			expectedStatus: http.StatusOK,
			expectedData: func() []model.Project {
				data := make([]model.Project, 1)
				data = append(data, model.Project{ShortName: "test", LongName: "test long"})
				return data
			},
		},
		"list projects with no data found": {
			query: "?name=test",
			find: func(name string) ([]model.Project, error) {
				data := make([]model.Project, 0)
				return data, nil
			},
			expectedStatus: http.StatusOK,
			expectedData: func() []model.Project {
				data := make([]model.Project, 0)
				return data
			},
		},
		"list projects with no data to search for": {
			query:          "?name=",
			expectedStatus: http.StatusBadRequest,
			expectedError:  "Missing name parameter\n",
		},
		"list projects with error in service": {
			query: "?name=test",
			find: func(name string) ([]model.Project, error) {
				return nil, fmt.Errorf("error")
			},
			expectedStatus: http.StatusBadRequest,
			expectedError:  "error\n",
		},
	}
	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			mock := service.MockProjectService{Find: tc.find}
			api := api.NewProjectAPI(&mock)
			router := mux.NewRouter()
			api.RegisterProjectRoutes(router)
			srv := httptest.NewServer(router)
			defer srv.Close()
			r, err := http.Get(srv.URL + "/projects" + tc.query)
			assert.Equal(t, tc.expectedStatus, r.StatusCode)
			if err != nil {
				t.Errorf("found unexpected error %v", err)
			}
			data, err := ioutil.ReadAll(r.Body)
			if err != nil {
				t.Errorf("found unexpected error %v", err)
			}
			if len(tc.expectedError) > 0 {
				assert.Equal(t, tc.expectedError, string(data))
			} else {
				var p []model.Project

				err = json.Unmarshal(data, &p)

				assert.Equal(t, len(tc.expectedData()), len(p))
				for i, project := range p {
					assert.Equal(t, tc.expectedData()[i].ShortName, project.ShortName)
					assert.Equal(t, tc.expectedData()[i].LongName, project.LongName)
				}
			}
		})
	}

}
