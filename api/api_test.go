package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

type mockHandler func(h http.ResponseWriter, r *http.Request) (interface{}, int, *Error)

type readBody func(d []byte) (interface{}, error)

type result struct {
	Message string `json:"message"`
}

func TestHandler(t *testing.T) {
	tt := map[string]struct {
		name            string
		call            mockHandler
		body            readBody
		expectedMessage string
		expectedStatus  int
		expectedData    interface{}
	}{
		"expect 200 with data": {
			call: func(h http.ResponseWriter, r *http.Request) (interface{}, int, *Error) {
				return result{Message: "test"}, 200, nil
			},
			body: func(d []byte) (interface{}, error) {
				var result result
				err := json.Unmarshal(d, &result)
				return result, err
			},
			expectedStatus: 200,
			expectedData:   result{Message: "test"},
		},
		"expect 500 with error": {
			call: func(h http.ResponseWriter, r *http.Request) (interface{}, int, *Error) {
				return nil, 500, NewError(nil, "just an error", 500)
			},
			body: func(d []byte) (interface{}, error) {
				return string(d), nil
			},
			expectedStatus: 500,
			expectedData:   "just an error\n",
		},
	}
	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			router := mux.NewRouter()
			router.Handle("/test", handle(tc.call)).Methods("GET")
			srv := httptest.NewServer(router)
			defer srv.Close()

			r, err := http.Get(srv.URL + "/test")
			if len(tc.expectedMessage) > 0 {
				assert.EqualError(t, err, tc.expectedMessage)
			} else if err != nil {
				t.Fatal(err)
			}
			assert.Equal(t, tc.expectedStatus, r.StatusCode)

			data, err := ioutil.ReadAll(r.Body)

			if err != nil {
				t.Errorf("found unexpected error %v", err)
			}
			result, err := tc.body(data)
			if err != nil {
				t.Errorf("found unexpected error %v", err)
			}
			assert.Equal(t, tc.expectedData, result)
		})
	}
}

func TestLogger(t *testing.T) {
	cw := newChanWriter()
	defer cw.Close()
	log.SetOutput(cw)
	call := func(h http.ResponseWriter, r *http.Request) (interface{}, int, *Error) {
		return result{Message: "test"}, http.StatusOK, nil
	}
	router := mux.NewRouter()
	router.Handle("/test", logger(handle(call), "test_handler")).Methods("GET")
	srv := httptest.NewServer(router)
	defer srv.Close()

	r, err := http.Get(srv.URL + "/test")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, http.StatusOK, r.StatusCode)
	assert.Contains(t, string(<-cw.Chan()), "test_handler")

	log.SetOutput(os.Stdout)
}

type chanWriter struct {
	ch chan []byte
}

func newChanWriter() *chanWriter {
	return &chanWriter{make(chan []byte, 1024)}
}

func (w *chanWriter) Chan() <-chan []byte {
	return w.ch
}

func (w *chanWriter) Write(p []byte) (int, error) {
	w.ch <- p
	return len(p), nil
}

func (w *chanWriter) Close() error {
	close(w.ch)
	return nil
}
