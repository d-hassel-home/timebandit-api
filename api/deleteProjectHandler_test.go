package api_test

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/d-hassel-home/timebandit-api/api"
	"gitlab.com/d-hassel-home/timebandit-api/service"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestDeleteProject(t *testing.T) {
	tt := map[string]struct {
		name           string
		data           string
		delete         func(uuid uuid.UUID) error
		expectedError  string
		expectedStatus int
	}{
		"test simple call to delete project": {
			data:           "d3ce797e-f409-4e26-a401-9b060c002627",
			delete:         func(uuid uuid.UUID) error { return nil },
			expectedStatus: http.StatusOK,
		},
		"test call delete with no uuid": {
			data:           "",
			expectedStatus: http.StatusMethodNotAllowed,
		},
		"test call delete with short uuid": {
			data:           "test",
			expectedStatus: http.StatusBadRequest,
			expectedError:  "invalid UUID length: 4\n",
		},
		"test call delete with non existing uuid": {
			data: "d3ce797e-f409-4e26-a401-9b060c002627",
			delete: func(uuid uuid.UUID) error {
				return fmt.Errorf("Faild to delete the dataset. UUID d3ce797e-f409-4e26-a401-9b060c002627 not found")
			},
			expectedStatus: http.StatusBadRequest,
			expectedError:  "Faild to delete the dataset. UUID d3ce797e-f409-4e26-a401-9b060c002627 not found\n",
		},
	}
	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			mock := service.MockProjectService{Delete: tc.delete}
			api := api.NewProjectAPI(&mock)
			router := mux.NewRouter()
			api.RegisterProjectRoutes(router)
			srv := httptest.NewServer(router)
			defer srv.Close()
			req, err := http.NewRequest("DELETE", srv.URL+"/projects/"+tc.data, nil)
			if err != nil {
				t.Errorf("Unexpected Error :\n %s ", err.Error())
			}
			client := &http.Client{}
			r, err := client.Do(req)
			assert.Equal(t, tc.expectedStatus, r.StatusCode)
			if err != nil {
				t.Errorf("Unexpected Error :\n %s ", err.Error())
			}
			data, err := ioutil.ReadAll(r.Body)
			if err != nil {
				t.Errorf("Unexpected Error :\n %s ", err.Error())
			}
			if len(tc.expectedError) > 0 {
				assert.Equal(t, tc.expectedError, string(data))
			}
		})
	}
}
