package api

import (
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"net/http"
)

func (p *projectAPI) getProject(w http.ResponseWriter, r *http.Request) (interface{}, int, *Error) {
	vars := mux.Vars(r)
	uid := vars["uuid"]
	id, err := uuid.Parse(uid)
	if err != nil {
		return nil, http.StatusBadRequest, NewError(err, err.Error(), http.StatusBadRequest)
	}
	project, err := p.service.GetProject(id)
	if err != nil {
		return nil, http.StatusBadRequest, NewError(err, err.Error(), http.StatusBadRequest)
	}
	if project == nil {
		return nil, http.StatusNotFound, nil
	}
	return project, http.StatusOK, nil
}
