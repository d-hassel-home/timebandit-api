package api

import (
	"github.com/gorilla/mux"
	"gitlab.com/d-hassel-home/timebandit-api/service"
)

type projectAPI struct {
	service service.ProjectService
}

type sendProjectDescription struct {
	Name        string `json:"name" valid:"required~name is a required field and is empty"`
	Description string `json:"description" `
}

type errorResponse struct {
	Message string   `json:"message"`
	Errors  []string `json:"errors,omitempty"`
}

// NewProjectAPI creates a new instance of this api using the given service for persistence
func NewProjectAPI(service service.ProjectService) projectAPI {
	h := projectAPI{service}
	return h
}

// RegisterProjectRoutes registers all project routes to the given router
func (p *projectAPI) RegisterProjectRoutes(router *mux.Router) {
	router.StrictSlash(true)

	router.Handle("/projects", logger(handle(p.createProject), "create project")).Methods("POST")
	router.Handle("/projects/{uuid}", logger(handle(p.getProject), "get project")).Methods("GET")
	router.Handle("/projects/{uuid}", logger(handle(p.deleteProject), "delete project")).Methods("DELETE")
	router.Path("/projects").Queries("name", "{name}").Handler(logger(handle(p.listProjects), "list projects")).Methods("GET")
}
