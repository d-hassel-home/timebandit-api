# STEP 1 build executable binary
FROM golang:alpine as builder
RUN apk update && apk add git
COPY . myapp/
WORKDIR myapp/
#get dependancies
ENV GIT_TERMINAL_PROMPT=1
# go get gitlab.com/d-hassel-home/timebandit-api/api'
#you can also use dep
RUN go get -d -v -t ./...
#build the binary
RUN CGO_ENABLED=0 GOOS=linux go build -v -a -installsuffix cgo -o /go/bin/main
# STEP 2 build a small image
# start from scratch
FROM scratch
# Copy our static executable
COPY --from=builder /go/bin/main /go/bin/main
EXPOSE 8080
ENTRYPOINT ["/go/bin/main"]
