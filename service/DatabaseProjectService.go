package service

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/d-hassel-home/timebandit-common/model"
	"log"
	"time"
)

// DatabaseProjectService ist the implementation of the projectService persisting the data in prostgesql database instances
type DatabaseProjectService struct {
	db      *sql.DB
	stdMnts []setupStatement
	insert  *sql.Stmt
	get     *sql.Stmt
	delete  *sql.Stmt
	find    *sql.Stmt
}

// NewProjectService creates a new instance of the persistance layer for projects in postgresql databases
func NewProjectService() *DatabaseProjectService {
	m := []setupStatement{
		{
			description: "load module pgcrypto",
			statement:   "CREATE EXTENSION IF NOT EXISTS pgcrypto;",
		},
		{
			description: "create table 'projects'",
			statement:   `CREATE TABLE IF NOT EXISTS projects(id UUID PRIMARY KEY DEFAULT gen_random_uuid(),short_name text NOT NULL, long_name text NOT NULL, created_at TIMESTAMPTZ DEFAULT now());`,
		},
	}
	var service = &DatabaseProjectService{stdMnts: m}
	return service
}

// Name provides the human readable name of this service
func (p *DatabaseProjectService) Name() string { return "Database Project Service" }

// SetupDbForService provides the setup for the database instance to be configured for this service.
// It performs all setup statements required to write, read and delete projects in the database.
func (p *DatabaseProjectService) SetupDbForService(r *DBRunner) error {

	p.db = r.Db

	return r.Transact(func(tx *sql.Tx) error {
		for _, update := range p.stdMnts {

			_, err := tx.Exec(update.statement)

			if err != nil {
				log.Printf("Action on: %s failed due to following error %v", update.description, err)
				return err
			}

		}
		return nil
	})
}

// CreateProject persists a new project with the given parameters and returns the result
func (p *DatabaseProjectService) CreateProject(shortName string, longName string) (*model.Project, error) {
	var project *model.Project
	rows, err := p.db.Query("INSERT INTO projects (short_name, long_name) values ($1, $2) RETURNING *", shortName, longName) //p.insert.Query(shortName, longName)
	if err != nil {
		return project, err
	}
	defer rows.Close()

	ok := rows.Next()
	if !ok {
		return project, errors.New("Project not correctly created because no new data found")
	}
	var uid string
	var sName, lName string
	var date time.Time

	err = rows.Scan(&uid, &sName, &lName, &date)
	uuid, err := uuid.Parse(uid)
	if err != nil {
		return project, err
	}
	project = &model.Project{UUID: uuid, ShortName: sName, LongName: lName, Created: date}

	return project, nil
}

// GetProject returns the project for the given uuid.
// nil will be returned if no project for the given uuid was found in the database
func (p *DatabaseProjectService) GetProject(id uuid.UUID) (*model.Project, error) {
	var project model.Project
	var uuidUnparsed, nShort, nLong string
	var date time.Time
	row := p.db.QueryRow("SELECT * FROM projects WHERE id = $1", id.String())
	err := row.Scan(&uuidUnparsed, &nShort, &nLong, &date)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			return nil, nil
		}
		return &project, err
	}
	uuidParsed, err := uuid.Parse(uuidUnparsed)
	if err != nil {
		return &project, err
	}
	project = model.Project{UUID: uuidParsed, ShortName: nShort, LongName: nLong, Created: date}
	return &project, nil
}

// DeleteProject will delete the project with the given uuid.
// If the project can not be deleted, because it does not exists or due to permission problems,
// an error will be returned.
func (p *DatabaseProjectService) DeleteProject(uuid uuid.UUID) error {
	res, err := p.db.Exec("DELETE FROM projects WHERE id = $1", uuid.String())
	if err != nil {
		return err
	}
	rows, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if rows == 0 {
		return fmt.Errorf("Faild to delete the dataset. UUID %s not found", uuid.String())
	}
	return nil
}

// FindProjects tries to find all projects with the name equal or starting with the given phrase
func (p *DatabaseProjectService) FindProjects(name string) ([]model.Project, error) {
	var projects []model.Project
	rows, err := p.db.Query("SELECT * FROM projects WHERE short_name like $1", name)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var uuidUnparsed, nShort, nLong string
		var date time.Time
		err := rows.Scan(&uuidUnparsed, &nShort, &nLong, &date)
		if err != nil {
			return nil, err
		}
		uuidParsed, err := uuid.Parse(uuidUnparsed)
		if err != nil {
			return nil, err
		}
		projects = append(projects, model.Project{UUID: uuidParsed, ShortName: nShort, LongName: nLong, Created: date})
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return projects, nil
}
