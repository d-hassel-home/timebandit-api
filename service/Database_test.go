package service

import (
	"database/sql"
	"errors"
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

type createAndRegisterMock func(statements []string) (*sql.DB, sqlmock.Sqlmock)

func registerDbSetUpToMock(statements []string) (*sql.DB, sqlmock.Sqlmock) {
	db, mock, _ := sqlmock.New()
	mock.ExpectBegin()
	for _, s := range statements {
		mock.ExpectExec(s).WillReturnResult(sqlmock.NewResult(0, 0))
	}
	mock.ExpectCommit()
	mock.ExpectClose()
	return db, mock
}

func registerDbWithErrorForBegin(statements []string) (*sql.DB, sqlmock.Sqlmock) {
	db, mock, _ := sqlmock.New()
	mock.ExpectBegin().WillReturnError(errors.New("db.begin failed"))
	return db, mock
}

func registerDbWithErrorInStatement(statements []string) (*sql.DB, sqlmock.Sqlmock) {
	db, mock, _ := sqlmock.New()
	mock.ExpectBegin()
	for _, s := range statements {
		mock.ExpectExec(s).WillReturnError(errors.New("Statement failed"))
	}
	mock.ExpectRollback()
	mock.ExpectClose()
	return db, mock
}

func registerDbWithPanicInStatement(statements []string) (*sql.DB, sqlmock.Sqlmock) {
	db, mock, _ := sqlmock.New()
	mock.ExpectBegin()
	for _, s := range statements {
		mock.ExpectExec(s).WillReturnError(errors.New("panic"))
	}
	mock.ExpectRollback()
	return db, mock
}

func registerNilDb(statements []string) (*sql.DB, sqlmock.Sqlmock) {
	_, mock, _ := sqlmock.New()
	mock.ExpectBegin()
	for _, s := range statements {
		mock.ExpectExec(s).WillReturnResult(sqlmock.NewResult(0, 0))
	}
	mock.ExpectClose()
	return nil, mock
}

func TestCloser(t *testing.T) {
	tt := map[string]struct {
		name               string
		registration       createAndRegisterMock
		statements         []setupStatement
		expectedStatements []string
		expectedExeption   string
		expectedError      error
	}{
		"Test with existing statements": {
			statements: []setupStatement{
				{description: "load module pgcrypto",
					statement: "CREATE EXTENSION IF NOT EXISTS pgcrypto;",
				},
				{description: "table 'projects'",
					statement: "CREATE TABLE IF NOT EXISTS projects( id UUID);",
				},
			},
			expectedStatements: []string{"REATE EXTENSION IF NOT EXISTS", "CREATE TABLE IF NOT EXISTS"},
			registration:       registerDbSetUpToMock,
		},
		"Test with no db instance": {
			statements: []setupStatement{
				{description: "load module pgcrypto",
					statement: "CREATE EXTENSION IF NOT EXISTS pgcrypto;",
				},
				{description: "table 'projects'",
					statement: "CREATE TABLE IF NOT EXISTS projects( id UUID);",
				},
			},
			expectedStatements: []string{"REATE EXTENSION IF NOT EXISTS", "CREATE TABLE IF NOT EXISTS"},
			registration:       registerNilDb,
			expectedExeption:   "Database driver cannot be nil",
		},
		"Test with transaction error": {
			expectedExeption: "Failed to setup the database connection for the Database Mock Service",
			registration:     registerDbSetUpToMock,
			expectedError:    errors.New("Failed to setup the database connection for the Database Mock Service"),
		},
		"Test with error for begin transaction": {
			expectedExeption: "db.begin failed",
			registration:     registerDbWithErrorForBegin,
		},
		"Test with error in statement": {
			statements: []setupStatement{
				{description: "load module pgcrypto",
					statement: "CREATE EXTENSION IF NOT EXISTS pgcrypto;",
				},
			},
			expectedStatements: []string{"REATE EXTENSION IF NOT EXISTS"},
			registration:       registerDbWithErrorInStatement,
			expectedExeption:   "Statement failed",
		},
		"Test with panic in statement": {
			statements: []setupStatement{
				{description: "load module pgcrypto",
					statement: "CREATE EXTENSION IF NOT EXISTS pgcrypto;",
				},
			},
			expectedStatements: []string{"REATE EXTENSION IF NOT EXISTS"},
			registration:       registerDbWithPanicInStatement,
			expectedExeption:   "panic",
		},
	}

	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {

			db, mock := tc.registration(tc.expectedStatements)

			defer func() {

				if p := recover(); p != nil {

					if err := mock.ExpectationsWereMet(); err != nil {
						t.Fatal(err)
					}
				}
			}()

			s := []DBService{DatabaseMockService{err: tc.expectedError, statements: tc.statements}}
			ps, err := New(s, WithDatabase(db))

			if err != nil {
				assert.EqualError(t, err, tc.expectedExeption)
			} else if len(tc.expectedExeption) > 0 {
				t.Fatalf("Expected Error to be : %s ", tc.expectedExeption)
			} else {
				ps.Close()
				if err := mock.ExpectationsWereMet(); err != nil {
					t.Fatal(err)
				}
			}

		})
	}

}

type DatabaseMockService struct {
	db         *sql.DB
	err        error
	statements []setupStatement
}

func (p DatabaseMockService) Name() string { return "Database Mock Service" }

func (p DatabaseMockService) SetupDbForService(r *DBRunner) error {
	p.db = r.Db

	if p.err != nil {
		return p.err
	}

	return r.Transact(func(tx *sql.Tx) error {
		for _, update := range p.statements {

			_, err := tx.Exec(update.statement)

			if err != nil {
				if err.Error() == "panic" {
					panic(err)
				} else {
					log.Printf("Action on: %s failed due to following error %v", update.description, err)
					return err
				}
			}

		}
		return nil
	})
}
