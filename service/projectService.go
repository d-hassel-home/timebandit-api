package service

import (
	"github.com/google/uuid"
	"gitlab.com/d-hassel-home/timebandit-common/model"
)

// ProjectService is used to communicate with the persistence layer of model.Project objects
type ProjectService interface {
	CreateProject(shortName string, longName string) (*model.Project, error)
	GetProject(uuid uuid.UUID) (*model.Project, error)
	DeleteProject(uuid uuid.UUID) error
	FindProjects(name string) ([]model.Project, error)
}
