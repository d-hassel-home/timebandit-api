package service

import (
	"database/sql"
	"fmt"
	// import postgresql driver
	_ "github.com/lib/pq"
	"log"
)

type setupStatement struct {
	description string
	statement   string
}

// StoreConfig to define the connection settings of the database instance
type StoreConfig func(*DBRunner, []DBService) error

// DBService with the default setup definition
type DBService interface {
	Name() string
	SetupDbForService(r *DBRunner) error
}

// DBRunner to execute all statements
type DBRunner struct {
	Db *sql.DB
}

//New to get a DBRunner with a selected set of services and a StoreConfig for the database settings
func New(services []DBService, config StoreConfig) (DBRunner, error) {
	runner := DBRunner{}
	err := config(&runner, services)
	if err != nil {
		return runner, err
	}
	return runner, nil
}

// WithDatabaseCredentials to establish and config a database connection with host, dbname, user and password
func WithDatabaseCredentials(dbUser, dbPassword, dbName, dbHost string) StoreConfig {
	return func(runner *DBRunner, stores []DBService) error {
		connectStr := fmt.Sprintf("user=%s password=%s host=%s dbname=%s sslmode=disable",
			dbUser, dbPassword, dbHost, dbName)

		db, err := sql.Open("postgres", connectStr)

		runner.Db = db

		if err != nil {
			return fmt.Errorf("Cannot connect to database server, please verify if config.js contains the right settings or db server is available, Error: %+v", err)
		}

		return setupDatabase(runner, stores)
	}
}

func setupDatabase(runner *DBRunner, stores []DBService) error {
	for _, s := range stores {
		e := s.SetupDbForService(runner)
		if e != nil {
			log.Printf("Failed to setup the database connection for the %v", s.Name())
			return e
		}
	}
	return nil
}

// WithDatabase toconfigure an existing database connection
func WithDatabase(db *sql.DB) StoreConfig {
	return func(runner *DBRunner, stores []DBService) error {

		if db == nil {
			return fmt.Errorf("Database driver cannot be nil")
		}
		runner.Db = db

		return setupDatabase(runner, stores)
	}
}

// Transact to perform each statement in one transaction
func (r DBRunner) Transact(txFunc func(*sql.Tx) error) error {
	tx, err := r.Db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if p := recover(); p != nil {
			log.Print("Rollack due to panic")
			tx.Rollback()
			panic(p) // re-throw panic after Rollback
		} else if err != nil {
			log.Print("Rollback due to an error")
			err = tx.Rollback()
		} else {
			err = tx.Commit()
		}
	}()
	err = txFunc(tx)
	return err
}

// Close the database connection
func (r *DBRunner) Close() error {
	return r.Db.Close()
}
