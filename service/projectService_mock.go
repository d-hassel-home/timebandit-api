package service

import (
	"github.com/google/uuid"
	"gitlab.com/d-hassel-home/timebandit-common/model"
)

type MockProjectService struct {
	Create func(shortName string, longName string) (*model.Project, error)
	Get    func(uuid uuid.UUID) (*model.Project, error)
	Delete func(uuid uuid.UUID) error
	Find   func(name string) ([]model.Project, error)
}

func (m *MockProjectService) CreateProject(shortName string, longName string) (*model.Project, error) {
	if m.Create == nil {
		panic("create not implemented")
	}
	return m.Create(shortName, longName)
}
func (m *MockProjectService) GetProject(uuid uuid.UUID) (*model.Project, error) {
	if m.Get == nil {
		panic("get not impemented")
	}
	return m.Get(uuid)
}
func (m *MockProjectService) DeleteProject(uuid uuid.UUID) error {
	if m.Delete == nil {
		panic("delete not implemented")
	}
	return m.Delete(uuid)
}
func (m *MockProjectService) FindProjects(name string) ([]model.Project, error) {
	if m.Find == nil {
		panic("find not implemented")
	}
	return m.Find(name)
}
