package service

import (
	"database/sql"
	"errors"
	"fmt"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestNewProjectService(t *testing.T) {
	s := NewProjectService()
	assert.Equal(t, 2, len(s.stdMnts))
	var db *sql.DB
	assert.Equal(t, db, s.db)
}

func TestName(t *testing.T) {
	n := NewProjectService().Name()
	assert.Equal(t, "Database Project Service", n)
}

type setupMock func(mock sqlmock.Sqlmock)
type preparation func(db *sql.DB) (*sql.Stmt, error)

func TestProjectServiceSetup(t *testing.T) {
	tt := map[string]struct {
		name          string
		setup         setupMock
		expectedError string
	}{
		"simple setup product service": {
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.ExpectExec("CREATE EXTENSION IF NOT EXISTS pgcrypto").WillReturnResult(sqlmock.NewResult(0, 0))
				mock.ExpectExec("CREATE TABLE IF NOT EXISTS projects").WillReturnResult(sqlmock.NewResult(0, 0))
			},
		},
		"setup product service with error": {
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.ExpectExec("CREATE EXTENSION IF NOT EXISTS pgcrypto").WillReturnError(errors.New("fails"))
			},
			expectedError: "fails",
		},
	}
	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			s := []DBService{NewProjectService()}
			db, mock, _ := sqlmock.New()
			tc.setup(mock)
			r, err := New(s, WithDatabase(db))
			r.Close()
			if err != nil {
				assert.EqualError(t, err, tc.expectedError)
			} else if err := mock.ExpectationsWereMet(); err != nil {
				t.Error(err)
			}
		})
	}

}

func TestCreateProject(t *testing.T) {

	cols := []string{"id", "short_name", "long_name", "created"}

	tt := map[string]struct {
		name              string
		pName             string
		pLong             string
		UUID              string
		expectedException string
		setup             setupMock
	}{
		"simple call test": {
			pName: "test",
			pLong: "test",
			UUID:  "d3ce797e-f409-4e26-a401-9b060c002627",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("INSERT INTO projects \\(short_name, long_name\\)").WillReturnRows(sqlmock.NewRows(cols).AddRow("d3ce797e-f409-4e26-a401-9b060c002627", "test", "test", time.Now().UTC()))
			},
		},
		"simple call test with unparsable uuid": {
			pName: "test",
			pLong: "test",
			UUID:  "d3ce797e-f409-4e26-a401-9b060c002627",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("INSERT INTO projects \\(short_name, long_name\\)").WillReturnRows(sqlmock.NewRows(cols).AddRow("fails", "test", "test", time.Now().UTC()))
			},
			expectedException: "invalid UUID length: 5",
		},
		"simple call test with error in query": {
			pName: "test",
			pLong: "test",
			UUID:  "d3ce797e-f409-4e26-a401-9b060c002627",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("INSERT INTO projects \\(short_name, long_name\\)").WillReturnError(errors.New("Invalid data type"))
			},
			expectedException: "Invalid data type",
		},
		"simple call test with no results": {
			pName: "test",
			pLong: "test",
			UUID:  "d3ce797e-f409-4e26-a401-9b060c002627",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("INSERT INTO projects \\(short_name, long_name\\)").WillReturnRows(sqlmock.NewRows(cols))
			},
			expectedException: "Project not correctly created because no new data found",
		},
	}

	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			s := NewProjectService()
			db, mock, _ := sqlmock.New()
			tc.setup(mock)
			s.db = db
			project, err := s.CreateProject(tc.pName, tc.pLong)
			if err != nil {
				assert.EqualError(t, err, tc.expectedException)
			} else if err := mock.ExpectationsWereMet(); err != nil {
				t.Error(err)
			} else {
				assert.Equal(t, tc.pName, project.ShortName)
				assert.Equal(t, tc.pLong, project.LongName)
				assert.Equal(t, tc.UUID, project.UUID.String())
			}
		})
	}
}

func TestGetProject(t *testing.T) {
	cols := []string{"id", "short_name", "long_name", "created"}
	tt := map[string]struct {
		name              string
		pName             string
		pLong             string
		UUID              string
		expectedException string
		setup             setupMock
	}{
		"simple call test": {
			pName: "test",
			pLong: "test",
			UUID:  "d3ce797e-f409-4e26-a401-9b060c002627",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT \\* FROM projects WHERE id").WillReturnRows(sqlmock.NewRows(cols).AddRow("d3ce797e-f409-4e26-a401-9b060c002627", "test", "test", time.Now().UTC()))
			},
		},
		"simple call test with unparsable uuid": {
			pName: "test",
			pLong: "test",
			UUID:  "d3ce797e-f409-4e26-a401-9b060c002627",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT \\* FROM projects WHERE id").WillReturnRows(sqlmock.NewRows(cols).AddRow("fails", "test", "test", time.Now().UTC()))
			},
			expectedException: "invalid UUID length: 5",
		},
		"simple call test with no data": {
			pName: "test",
			pLong: "test",
			UUID:  "d3ce797e-f409-4e26-a401-9b060c002627",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT \\* FROM projects WHERE id").WillReturnRows(sqlmock.NewRows(cols))
			},
		},
	}

	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			s := NewProjectService()
			db, mock, _ := sqlmock.New()
			tc.setup(mock)
			s.db = db
			uuid, _ := uuid.Parse(tc.UUID)
			project, err := s.GetProject(uuid)
			if err != nil {
				assert.EqualError(t, err, tc.expectedException)
			} else if err := mock.ExpectationsWereMet(); err != nil {
				t.Error(err)
			} else if project != nil {
				assert.Equal(t, tc.pName, project.ShortName)
				assert.Equal(t, tc.pLong, project.LongName)
				assert.Equal(t, tc.UUID, project.UUID.String())
			}
		})
	}
}

func TestDeleteProject(t *testing.T) {
	tt := map[string]struct {
		name              string
		UUID              string
		expectedException string
		setup             setupMock
	}{
		"simple call test": {
			UUID: "d3ce797e-f409-4e26-a401-9b060c002627",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectExec("DELETE FROM projects WHERE").WillReturnResult(sqlmock.NewResult(0, 1))
			},
		},
		"simple call test with no data found": {
			UUID: "d3ce797e-f409-4e26-a401-9b060c002627",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectExec("DELETE FROM projects WHERE").WillReturnResult(sqlmock.NewResult(0, 0))
			},
			expectedException: "Faild to delete the dataset. UUID d3ce797e-f409-4e26-a401-9b060c002627 not found",
		},
		"simple call test with error": {
			UUID: "d3ce797e-f409-4e26-a401-9b060c002627",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectExec("DELETE FROM projects WHERE").WillReturnError(fmt.Errorf("fails"))
			},
			expectedException: "fails",
		},
	}

	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			s := NewProjectService()
			db, mock, _ := sqlmock.New()
			tc.setup(mock)
			s.db = db
			uuid, _ := uuid.Parse(tc.UUID)
			err := s.DeleteProject(uuid)
			if err != nil {
				assert.EqualError(t, err, tc.expectedException)
			} else if err := mock.ExpectationsWereMet(); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestFindProjects(t *testing.T) {
	cols := []string{"id", "short_name", "long_name"}
	tt := map[string]struct {
		name              string
		pName             string
		UUID              string
		rowCount          int
		expectedException string
		setup             setupMock
	}{
		"simple call test": {
			pName:    "test",
			UUID:     "d3ce797e-f409-4e26-a401-9b060c002627",
			rowCount: 1,
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT \\* FROM projects WHERE short_name").WillReturnRows(sqlmock.NewRows(cols).AddRow("d3ce797e-f409-4e26-a401-9b060c002627", "test", "test"))
			},
		},
		"call find with multiple results": {
			pName:    "test",
			UUID:     "d3ce797e-f409-4e26-a401-9b060c002627",
			rowCount: 2,
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT \\* FROM projects WHERE short_name").WillReturnRows(sqlmock.NewRows(cols).AddRow("d3ce797e-f409-4e26-a401-9b060c002627", "test", "test").AddRow("d3ce797e-f409-4e26-a401-9b060c002627", "test", "test"))
			},
		},
		"call find projects with error": {
			pName: "test",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT \\* FROM projects WHERE short_name").WillReturnError(fmt.Errorf("fails"))
			},
			expectedException: "fails",
		},
		"call find projects with row error": {
			pName: "test",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT \\* FROM projects WHERE short_name").WillReturnRows(sqlmock.NewRows(cols).RowError(1, fmt.Errorf("fails")))
			},
			expectedException: "fails",
		},
		"simple call test with unparsable uuid": {
			pName: "test",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT \\* FROM projects WHERE short_name").WillReturnRows(sqlmock.NewRows(cols).AddRow("fails", "test", "test"))
			},
			expectedException: "invalid UUID length: 5",
		},
	}

	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			s := NewProjectService()
			db, mock, _ := sqlmock.New()
			tc.setup(mock)
			s.db = db
			projects, err := s.FindProjects(tc.pName)
			if err != nil {
				assert.EqualError(t, err, tc.expectedException)
			} else if err := mock.ExpectationsWereMet(); err != nil {
				t.Error(err)
			} else {
				assert.Equal(t, tc.rowCount, len(projects))
				for _, project := range projects {
					assert.Equal(t, tc.pName, project.ShortName)
					assert.Equal(t, tc.UUID, project.UUID.String())
				}
			}
		})
	}
}
